/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Yio
 *
 * Created on 30 de junio de 2020, 06:12 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * ESTRUCTURA DE DATOS FIFO COLA
 */


//ESTRUCTURA NODO

typedef struct Nodo {
    int valor;
    struct Nodo *siguiente;
} Nodo;


//PROTOTIPADO
Nodo* crearNodo(int valor);
void insertar(Nodo **p, Nodo**u, int valor);
void desplegarCola(Nodo *primero);
void buscarNodo(Nodo *primero, int valor);
void eliminarNodo(Nodo *primero, int valor);

/*apuntadores
Nodo *primero = NULL;
Nodo *ultimo = NULL;*/

int main() { 
    int opc = 0;
    do {
        printf("\n-----------------------------------------------\n");
        printf("\n                  COLA                         \n");
        printf("\n 1.INSERTAR     |    2.BUSCAR                  \n");
        printf("\n 3.MODIFICAR    |    4.ELIMINAR               \n");
        printf("\n 5.IMPRIMIR     |    6.SALIR                  \n");
        printf("\n-----------------------------------------------\n");
        printf("ELIJE UNA OPCION \n");
        scanf("%d", &opc);
        switch (opc) {
            case 1:
                printf("INSERTANDO NODO... \n");
                Nodo *primero = NULL;
                Nodo *ultimo = NULL;
                insertar(&primero, &ultimo, 13);
                insertar(&primero, &ultimo, 1);
                insertar(&primero, &ultimo, 5);
                insertar(&primero, &ultimo, 14);
                insertar(&primero, &ultimo, 43);
                insertar(&primero, &ultimo, 98);
                printf("OK, NODOS INSERTADOS... \n");
                break;
            case 2:
                printf("BUSCANDO NODOS... \n");
                printf("NODOS ENCONTRADOS... \n");
                buscarNodo(primero, 1);
                buscarNodo(primero, 14);
                break;
            case 3:
                printf(" MODIFICANDO NODO... \n");
                break;
            case 4:
                printf(" ELIMINANDO NODO... \n");
                 eliminarNodo(primero, 5);
                break;
            case 5:
                printf(" IMPRIMINEDO COLA... \n");
                desplegarCola(primero);
                break;
            case 6:
                printf(" OK, HASTA PRONTO... \n");
                break;
            default:
                printf(" ERROR AL INGRESAR VALOR \n");
        }
    } while (opc != 0 && opc < 6);

    return (EXIT_SUCCESS);
}

//FUNCIONES

Nodo* crearNodo(int valor) {
    Nodo *nuevo = NULL;
    nuevo = (Nodo*) malloc(sizeof (Nodo));
    if (nuevo != NULL) {
        nuevo->valor = valor;
        nuevo->siguiente = NULL;
    }
    return nuevo;
}

void insertar(Nodo **primero, Nodo **ultimo, int valor) {
    Nodo *nuevo;
    nuevo = crearNodo(valor); //creando Nodo  
    if (*primero == NULL) {
        *primero = nuevo;
        *ultimo = nuevo;
    } else {
        Nodo *aux = *ultimo;
        aux->siguiente = nuevo;
        *ultimo = nuevo;
    }
}

void desplegarCola(Nodo *primero) {
    Nodo *aux = primero;
    while (aux != NULL) {
        printf("%d ", aux->valor);
        aux = aux->siguiente;
    }
    printf("NULL");
}

void buscarNodo(Nodo *primero, int valor) {
    Nodo *aux;
    aux = primero;
    while (aux != NULL) {
        if (aux->valor == valor) {
            printf("\n %d esta en la cola ", aux->valor);
        }
        aux = aux->siguiente;
    }
    free(aux);
}

void eliminarNodo(Nodo *primero, int valor) {
    Nodo *eliminar = primero;
    Nodo *anterior;
    while (eliminar != NULL) { 
        if (eliminar->valor == valor) {
            anterior->siguiente = eliminar->siguiente;
            printf("%d se eliminó \n ",valor);
            free(eliminar); 
        }
        anterior = eliminar;
        eliminar = eliminar->siguiente;
    }  
    desplegarCola(primero);
}