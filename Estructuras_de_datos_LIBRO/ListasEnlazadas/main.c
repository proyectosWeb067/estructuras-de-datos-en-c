/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Yio
 *
 * Created on 14 de junio de 2020, 12:08 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

//---------------------------Struct--------------------------------------------

typedef struct Nodo {
    int valor; //Struct Nodo
    struct Nodo* siguiente;
} Nodo;
//-----------------------------------------------------------------------------


//-----------------------Defincion de funciones--------------------------------
Nodo* CrearNodo(int valor); //Definiendo funcion crear nodo
Nodo *InsertarInicio(Nodo **cabeza, int valor); //insertando al inicio
void ImprimirLista(Nodo *cabeza); //imprimiendo lista
int InsertarFinal(Nodo **c, int valor); //Insertando al final
void LimpiarMemoria(Nodo *nodo); // Limpia memoria
int ListaVacia(Nodo *cabeza); //Lista vacia 
Nodo *EncontrarElemento(Nodo *cabeza, int valor); //encontrar Nodos
void EliminarElemento(Nodo *cabeza, int valor);


//-----------------------------------------------------------------------------

int main() {
    Nodo *cabeza = NULL;
    Nodo *nodoInsertado; //me devuelve la  posicion en memoria del primer nodo insertao

    nodoInsertado = InsertarInicio(&cabeza, 3); //envio la posicion en memoria de cabeza

    nodoInsertado = InsertarInicio(&cabeza, 98);

    InsertarFinal(&cabeza, 20);
    InsertarFinal(&cabeza, 5);

    printf(" Antes de eliminar.. \n");
    ImprimirLista(cabeza);
    printf("\n Despues de eliminar..\n");
    EliminarElemento(cabeza, 5);

    return (EXIT_SUCCESS);
}


//------------------------Implementacion de funciones--------------------------

//funcion para que crea un nodo

Nodo* CrearNodo(int valor) {
    Nodo* nuevo = NULL;
    nuevo = (Nodo*) malloc(sizeof (Nodo)); //memoria dinamica para el nodo

    if (nuevo != NULL) {
        nuevo->valor = valor;
        nuevo->siguiente = NULL;
    }
    return nuevo;
}

//funcion que Inserta al inicio

Nodo *InsertarInicio(Nodo** cabeza, int valor) { //Nodo** puntero doble que almacena posicion de memoria de un puntero Nodo*
    Nodo* nuevo = NULL;
    nuevo = CrearNodo(valor);
    if (nuevo != NULL) {
        nuevo->siguiente = *cabeza;
        *cabeza = nuevo;
        return *cabeza; // boolean
    }
}

//funcion que Inserta al final

int InsertarFinal(Nodo **c, int valor) {
    Nodo *nuevo = NULL;
    nuevo = CrearNodo(valor);
    Nodo *inicial = *c;
    if (nuevo != NULL) {
        while (inicial->siguiente != NULL) {
            inicial = inicial->siguiente;
        }
        inicial->siguiente = nuevo;
        return 0;
    }
    return 0;
}

//funcion que imprime lista

void ImprimirLista(Nodo *cabeza) {
    Nodo *aux = cabeza;
    while (aux != NULL) {
        printf("%d --> ", aux->valor);
        aux = aux->siguiente;
    }
    printf("NULL ");
}

//funcion que limpia la memoria 

void LimpiarMemoria(Nodo *nodo) {
    free(nodo);
}

//funcion que verifica si la a lista esta vacia

int ListaVacia(Nodo *cabeza) {
    if (cabeza == NULL) {
        return 1;
    } else {
        return 0;
    }
}

//funcion que encuentra un nodo en la lista

Nodo *EncontrarElemento(Nodo *cabeza, int valor) {
    Nodo *aux = cabeza;

    if (aux != NULL) {
        while (aux->valor == valor) {
            printf("\n elemento %d en lista", aux->valor);
            return aux;
            aux = aux->siguiente;
        }
        printf("\n elemento no existe");
    }
}

//funcion que elimina un elemento

void EliminarElemento(Nodo *cabeza, int valor) {
    Nodo *posInicial = cabeza;
    //eliminando al principio
    if (posInicial->valor == valor && posInicial->siguiente != NULL) {
        cabeza = posInicial->siguiente;
        free(posInicial);
        ImprimirLista(cabeza);
    } else if (posInicial != NULL) { //eliminando al final 
        Nodo *inicial = posInicial;
        Nodo *anterior;
        while (posInicial != NULL) {
            if (posInicial->valor == valor && posInicial->siguiente == NULL) {
                LimpiarMemoria(posInicial);
                anterior->siguiente = NULL;
            } else {
                anterior = posInicial;
            }
            posInicial = posInicial->siguiente;
        }
        ImprimirLista(inicial);
    }
}
 