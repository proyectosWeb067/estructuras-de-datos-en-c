/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Yio
 *
 * Created on 30 de junio de 2020, 02:13 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

//Estructura de datos LIFO - ultimo en entrar-primero en salir

typedef struct Nodo {
    int valor;
    struct Nodo* siguiente;
} Nodo;


Nodo* CrearNodo(int valor);
void poner(Nodo **cabeza, int valor); //Push
int sacar(Nodo** cabeza); //Pop
void pilaVacia(Nodo *n);
void Imprimir(Nodo *cabeza);

int main() {
    int opcion = 0;
    do {
        printf("\n---------------------------------------------------\n");
        printf("         ESTRUCTURA PILA                   \n ");
        printf("1.CREAR NODO    |      2.PUSH(PONER)       \n");
        printf("3.POP(SACAR)    |      4.VERIFICAR LA PILA ESTA VACIA \n");
        printf("5.IMPRIMIR      |      6.SALIR \n");
        printf("-----------------------------------------------------\n");
        printf("CREA UN NODO  \n");
        scanf("%d", &opcion);
        switch (opcion) {
            case 1:
                printf("CREANDO NODO...\n");
                Nodo *cabeza = NULL;
                poner(&cabeza, 4);
                poner(&cabeza, 54);
                printf("OK, NODO CREADO.");
                break;
            case 2:
                printf("PUSH \n");
                poner(&cabeza, 78);
                printf("PUSH OK");
                break;
            case 3:
                printf("POP \n");
                sacar(&cabeza);
                printf("POP OK");
                break;
            case 4:
                printf("VERIFICANDO PILA..\n");
                pilaVacia(cabeza);
                break;
            case 5:
                printf("IMPRIMIENDO PILA..\n");
                Imprimir(cabeza);
                break;
            case 6:
                printf("CERRANDO...\n");
                printf("OK, HASTA PRONTO");
                break;

            default:
                printf("LA OPCION NO ES CORRECTA, INTENTELO DE NUEVO");
                break;
        }
    } while (opcion != 0 && opcion < 6);
    return (EXIT_SUCCESS);
}

Nodo* CrearNodo(int valor) {
    Nodo *nuevo = NULL;
    nuevo = (Nodo*) malloc(sizeof (Nodo));
    if (nuevo != NULL) {
        nuevo->valor = valor;
        nuevo->siguiente = NULL;
    }
    return nuevo;
}

void poner(Nodo** cabeza, int valor) {
    Nodo *nuevo;
    nuevo = CrearNodo(valor);
    if (nuevo != NULL) {
        nuevo->siguiente = *cabeza;
        *cabeza = nuevo;
    }
}

int sacar(Nodo** cabeza) {
    Nodo *aux = *cabeza;
    int valor = aux->valor;
    *cabeza = aux->siguiente;
    free(aux);
    return printf("Sacaste el %d\n", valor);
}

void pilaVacia(Nodo *cabeza) {
    if (cabeza == NULL) {
        printf("La estrucutura esta vacia \n");
    }else{
        printf("La estrucutura contiene elementos \n");
    }
    
}

void Imprimir(Nodo *cabeza) {
    Nodo *n = cabeza;
    while (n != NULL) {
        printf("%d ", n->valor);
        n = n->siguiente;
    }
    printf("NULL");
}